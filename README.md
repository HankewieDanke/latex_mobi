# LaTeX_MoBi

Short introduction into LaTeX for MoBi Students in Heidelberg.
Repo provides a quick and dirty LaTeX in Overleaf tutorial and templates students may
through their studies.

----

LaTeX is not a program persey, it is a *markup* language similar to html, (R)markdown and so on.
Here we will provide you with programs/websites to use LaTeX and templates to get started.

You will also find tips and programs how to keep/organise a Bibliography for usage with LaTeX.


## I want my documents to wear LaTeX, but where do I start?

For the beginning we suggest to start on [Overleaf](https://www.overleaf.com/).
Overleaf allows for editing and compiling (ie. producing the pdf) in the browser.
As such you do not have to install LaTeX yourself, unless you feel adventures.
Then suggest: MiKTeX on Windos; MacTex for Mac; apt, pac & co. for linux.
A further benefit of Overleaf is, that it allows for collaborative editing similar to
google docs.
In the following we assume that you are working on Overleaf.
<img src="./tutorial_files/overleaf.jpg" width="50%" height="50%">


# Overleaf the basics

Start as follows:

1. Navigate to the folder of the template you want to use inside this repo
2. In your overleaf start page click new project and select upload project,
   drag/place the zip into the pop-up window.
3. You can now start working on the project.

Alternatively one can link gitlab/hub repos to overleaf, if you want to do this.

To compile a project with overleaf simply click compile in the upright corner.
To include new images in your project upload them and place them into the image folder of the project.


# LaTeX the basics.

__Let's start with a simple file:__ 

``` latex
\documentclass{article}

\begin{document}
    some text 
\end{document}
```

## The document class

+ first line of the document defines the class of the document in the curved brackets
+ the document class comes with a predefined format
+ this format can be adjusted later on

So beginning your document you can choose out of several layouts: 

+ report
+ article
+ book
+ letter
+ slides
+ beamer

...and others.
In order to see how changing the class influences our layout, you can take the document_class.tex file from the start_templates folder and put it into your overleaf. 
By changing the class in the curved brackets to one of the above classes, you can investigate how the layout changes. 

## commands, arguments, options and environments

Let's move on to the syntax of TeX. 

``` latex
\documentclass{article}

\begin{document}
    some text 
\end{document}
```

__Commands__ are the tools with which you can format and edit your document. ´´documentclass´´ is such a command. 
Commands take as an input __arguments__ and __options__. 
The __arguments__ need to be written into the curved brackets and the __options__ are written into square brackets.
In our example file documentclass is the command. Article defines the class of the document and is thereby the given argument. 
Additionally, options can be specified, describing additional properties of the documents.
Like in the example below.

``` latex
\documentclass[12pt]{article}

\begin{document}
    some text 
\end{document}
```

__Environments.__ Till now, we defined the layout of our file by setting the document class and additional options. 
Now to actually write into our document, the document environment needs to be opened. 
All the content (text, tables, figures etc.) needs to be inserted in this document environment. 
The content inside this document environment, will be formated according to the layout we set in the lines above. 

+ So Environments are format blocks
+ in order to define other formats, like figures, tables, formulas etc. we need to open additional environments

``` latex
\documentclass[12pt]{article}

\begin{document}
    A forumla


    \begin{align}
        f(x) = x^2
    \end{align}


\end{document}
```

__Structure your LaTeX project.__

It is possible to create your essay by writing your report in one single ```.tex``` file. However, LaTeX makes it very convenient for you to structure you project better! Generally, we recommend to outsource all configurations, imports etc. into separate files, which we name ```main.tex``` and ```setup.tex```. Also, you can separate your text in different sections, which we will save in different  ```introduction.tex``` files ```methods.tex```, ```results.tex```, ```discussion.tex```. All of these sections are save in the folder ```chapter```. The sections are then imported into ```main.tex```. For ```introduction.tex``` for instance the command would be ```\input{./chapter/introduction}```.

## Structure your text

LaTeX offers the possibility to organize your content into different chapters and sections. The exact sections depend on the document class. However, most likely you will only need the commands given below. A possible structure of your essay could be as shown belWow:

``` latex
\documentclass[12pt]{article}

\begin{document}

\tableofcontents

    \section*{Abstract}
    \section{Introduction}
    \section{Methods}
    \subsection{RNA-sequencing}
    \subsection{SNP calling}
    \section{Results}
    \subsubsection{TP53 is a signal for cancer development}
    \paragraph{p53 is a transcription factor}
    \subparagraph{Regulation of cell cycle}
    \subparagraph{Induction of apoptosis}
    \section{Discussion}

\end{document}
```

The headlines are numbered automatically according to their level. Headlines with an asterix after the command (e.g. ```\section*{Abstract}```) are not numbered. The command ```\tableofcontents``` inserts an ordered table of content with all of the headlines and the respective page number.


## Add text formatting

Formatting with LaTeX is super convenient. Bold formating can be done with ```\textbf{}```, italic with ```\textit{}``` and ```\underline{}``` is used to underline text. Please see the following example below:

``` latex
\documentclass[12pt]{article}

\begin{document}

\tableofcontents

    Some of the \textbf{greatest} 
    discoveries in \underline{science} 
    were made by \textbf{\textit{accident}}.

\end{document}
```

## Display mathematical formulas

LaTeX is well known for the great possibilities to display mathematical formulas. If you compare the capabilities and ease of LaTeX with other tools such as Word for mathematical formula generation and display you will be amazed! Generally, there are different methods to display mathematical formulas in LaTeX. However, we want to suggest two main methods.

<ol>
<li>For in-line formulas the easiest way is to surround your formula with two "$"</li>

``` latex
\documentclass[12pt]{article}

\begin{document}
    
    In physics, the mass-energy equivalence is stated 
    by the equation $E=mc^2$, discovered in 1905 by Albert Einstein.

\end{document}
```
<li>For full-line formulas the easiest way is to make use of the equation environment.The use of a label is optional.</li>

``` latex
\documentclass[12pt]{article}

\begin{document}
    
    In physics, the mass-energy equivalence is stated 
    by the equation 

    \begin{equation}
        \label{formula:einstein}
        E=mc^2
    \end{equation}

    , discovered in 1905 by Albert Einstein. 
    
    As already shown in formula \ref{formula:einstein}

\end{document}
```
</ol>

In mathematical formulas you often find non-standard symbols, such as greek letters. [Here](https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols) you can find an easy overview of mathematical formulas in LaTeX. It can be tricky to LaTeX formulas right per hand. Online you can find services which offer you a graphical display to type in a forumla such as [this service](https://latex.codecogs.com/eqneditor/editor.php). Then you only need to copy the LaTeX code.

## Inserting & formatting a table

Generating a table can be a bit tricky at the beginning. An example for a table is shown below. Please also note that the sample table is designed to fulfill the IPMB criteria for a standard table. Also it does contain a caption and a label.

``` latex
\documentclass[12pt]{article}

\begin{document}

    In the table \ref{tab:Thisisadummytable} something is displayed.
    
    \begin{table}[]%hbpt!H are options for figure placement
        \centering
        \caption{This is a dummy table. You can use it as template for other tables. Remember caption goes on top because figure is bottom.}
        \label{tab:Thisisadummytable}
        \begin{tabular}{l|cc} % l, r,c are options for alignments of a column, a | introduces horizontal lines.
            \toprule
            I       &   am   &   Head    \\
            \midrule
            value   &   1    &   2       \\
            value   &   1    &   2       \\
            value   &   1    &   2       \\
            \bottomrule
        \end{tabular}
    \end{table}

\end{document}
```

With the ```\ref{table:*name_this*}``` and ```\label{table:*name_this*}``` you can mention in your text the table you are referring to. If you feel like it can be hard to get the layout of the table right it is also possible to use services such as [tablesgenerator.com](https://www.tablesgenerator.com/). Here you only need to upload a .csv file (for example for a table you created in Excel) or insert the content manually. However, please make sure to follow the IPMB guidelines (caption, hlines, ...)!

## Inserting a figure

Inserting a figure is super easy again! An example for a figure is shown below. LaTeX supports all kind of graphics formats - whether it is a .jpg, .png or .svg you are good to go. However, you may need to adjust the scale in the command ```\includegraphics[width=.5\textwidth]{fig/path.png}```. Please also note that this sample figure is designed to fulfill the IPMB criteria for a standard figure. Also it does contain a caption and a label.

``` latex
\documentclass[12pt]{article}

\begin{document}

    \begin{figure}[H]
        \centering
        \includegraphics[width=.5\textwidth]{fig/path.png}
        \caption{\textbf{Caption for this figure with images}. Further explanation.}
        \label{fig:universe}
    \end{figure}

    As you can see in the figure \ref{fig:universe}, the 
    function grows near 0. Also, in the page \pageref{fig:universe} 
    is the same example.

\end{document}
```

Also a figure with two subfigures is shown here:

``` latex
\documentclass[12pt]{article}

\begin{document}

    \begin{figure}[H]

        \begin{subfigure}{0.5\textwidth}
        \caption{}
        \includegraphics[width=0.9\linewidth, height=5cm]{universe} 
        \label{fig:subim1}
        \end{subfigure}
        \begin{subfigure}{0.5\textwidth}
        \caption{}
        \includegraphics[width=0.9\linewidth, height=5cm]{universe}
        \label{fig:subim2}
        \end{subfigure}
        
        \caption{\textbf{Caption for this figure with images}. Further explanation.}
        \label{fig:image}
    \end{figure}

\end{document}
```

The positioning of a figure can be a bit tricky. You can influence this by chaning the specifier H to h/t/b/p! in this command: ```\begin{figure}[H]```. For an exact explanation what each specifier does please refer to this [resource](https://de.overleaf.com/learn/latex/Positioning_of_Figures). With the ```\ref{fig:*name_this*}``` and ```\label{fig:*name_this*}``` you can mention in your text the figure you are referring to. Please note, that the subfigures also have labels, which can be referred to.

## references and Bibliography

+ how is it integrated into the file
+ simple example
+ \cite commad

# Bibtex the basics.
Citing is easy, in LaTeX every citation is assigned a unique name (label) either
by you, your Citation manager or the publisher.
When citing simply use `\cite{theLabel}` and Bibtex (or overleaf) will place your citation here.

Everyone uses different Citation managers.
The easiest to use in combination to with LaTeX are either
those where you can export the bibliography as bib file or
those that use a bib file as database (such as BibDesk on mac and JabRef on Windows/Linux).

Either way whatever you use try to avoid editing of the bib files by hand,
this tends to introduce errors by missing brackets and such.
If you add a new citation to you bibliography watch out for special letters
such as ä,ö,ü,é.
In some cases the bib files are badly formatted by the publisher or by the program
you use for import of the citation.
If you keep this in mind, most errors in compiling resulting from the bib. are solvable
by simply googling the latex command for the letter and replacing.

## Citation Manager (Zotero) and Overleaf

+ Download and basic usage of Zotero
+ adding your bibliography to your overleaf

### Setting up Zotero & basic usage

<img src="./tutorial_files/zotero_online_download.png" width="50%" height="50%">


1. Download & install Zotero
    + go to the official [zotero website](https://www.zotero.org/download/)
    + click on download for Zotero 6 and install the programm after the donwload
    + install the plugin for firefox (not really necessary but extremly useful)
    + click Register in the top right corner and set up a zotero account

Choosing a publication and clicking on the add on button for zotero in the right top corner:

<img src="./tutorial_files/zotero3.png" width="50%" height="50%">

Or copying the pubmed id or doi from the website,

<img src="./tutorial_files/zotero4.png" width="50%" height="50%">

and searching the publication via the search bar. 

<img src="./tutorial_files/zotero5.png" width="50%" height="50%">

2. basic usage
    + open zotero programm
    + go to google scholar and search for a random paper 
    + adding the paper to your library by
        - klicking the add On button in the firefox window and choosing the folder you want the paper to be in 
        - or copying the doi,pubmed id and putting it into the search bar in zotero
    + go back to your zotero window
        - go to edit > Preferences > Sync, click on "Link Account" and login into your account
        - if that work, close the preferences Window
        - do a right click on "Library" and click sync, to synchronize the added publications with your online account

Synchronizing your Account with your local Library on your computer:

<img src="./tutorial_files/2_zotero.png" width="50%" height="50%">

3. check if the synchronization works
    + go to the zotero website
    + log in 
    + click on your profile name
    + click on Library
    + is the publication, which you added already there, if yes: great! move on to step 3. If not, do the syncrhonization again, somethimes it takes a little bit


4. using your new library with zotero
    + go to overleaf & sign in, open an existing or a new document template
    + click on the upload button, in the left top corner under "Menu"
    + go to the submenu From Zotero
    + give your reference file a name
    + click on create


<img src="./tutorial_files/zotero6.png" width="50%" height="50%">
<img src="./tutorial_files/zotero7.png" width="50%" height="50%">


Each time you want to update your references file, you need to synchronize the library in your zotero programm (like described above), as well as press the refresh button at the top of your reference file in the created reference file in overleaf. 
In order to know what to write into your cite command for each citation, you can go into the created reference file. 
Each reference entry is structured like that:
```
@type_of_reference{...}
```
The first line of every entry is the alias of the reference which you can put into the cite command (without the commas of course!!)



